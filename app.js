const express = require('express')
const app = express()
const port = 3000

/**
 * This function adds two number together
 * @param {Number} a 
 * @param {Number} b
 * @returns {Number} 
 */
const add = (a,b) => {
    return a + b;
}

/** Path to add function*/

app.get('/add/', (req, res) => {
    const x = add(1,2);
    res.send(`Sum: ${x}`);
  })

  app.get('/dadd/', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    console.log({isANaN: isNaN(a), isBNaN: isNaN(b)});
    if (isNaN(a) || isNaN(b)) {
        res.send(`params a: ${a}, b: ${b}`);
    } else {
        const sum = add(a,b);
        res.send(`Sum is: ${sum.toString()}`);
    }
  })

  app.get('/', (req, res) => {
    res.send('Hello World!')
  })
    
app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}`)
})